import numpy as np


#Problem: https://codingcompetitions.withgoogle.com/kickstart/round/0000000000050e01/00000000000698d6

#Solution 1
def min_cost_1(num_list, P):
    num_list = np.sort(num_list)
    n_slide = len(num_list) - P + 1
    min_list = [0]*n_slide
    head = 0
    for n in range(0, n_slide):
        tail = head+P
        num_slide = num_list[head:tail]
        max_num_list = [num_slide[-1]]*P
        head = head + 1
        min_list[n] = np.array(max_num_list - num_slide).sum()

    return np.array(min_list).min()

#Solution 2
def min_cost_2(num_list, P):
    num_list = np.sort(num_list)
    n_slide = len(num_list) - P + 1
    min_score = 10000 * (P-1)
    head = 0
    for n in range(0, n_slide):
        tail = head+P
        num_slide = num_list[head:tail]
        max_num_list = [num_slide[-1]]*P
        head = head + 1
        score = np.array(max_num_list - num_slide).sum()
        if score < min_score:
            min_score = score

    return min_score

def main():
    T = int(input())
    for t in range(0, T):
        N, P = map(int, input().split(" "))
        S_list = np.array(list(map(int, input().split(" "))))
        min_score = min_cost_2(S_list, P)
        print("Case #{}: {}".format(t, min_score))


if __name__ == "__main__":
    main()